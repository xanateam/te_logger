import os
import inspect
import logging


# TODO change the splitter to reflect the right path
def get_analysis_root_path(filepath):
    """
    return the full path of a given path
    :type filepath: sting
    :return: string
    """
    complete_path = os.getcwd()
    root_path = os.path.join(complete_path.split('analysis')[0], 'analysis', filepath)
    return root_path


caller = inspect.currentframe().f_back
caller_name = caller.f_globals['__name__']
filename = caller_name.split('.')[0]

log_path = get_analysis_root_path("logs/{}.log".format(filename))
# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M',
                    filename=log_path,
                    filemode='a+')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)

# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')

# tell the handler to use this format
console.setFormatter(formatter)

# add the handler to the root logger
logging.getLogger('').addHandler(console)

log = logging.getLogger(caller_name)
